# Improving the visibility of minorities through network growth interventions


This repo contains the code to reproduce the experiments  presented in our paper: 

*Neuhäuser, L., Karimi, F., Bachmann, J. et al. Improving the visibility of minorities through network growth interventions. Communication Physics 6, 108 (2023). doi:10.1038/s42005-023-01218-9*

A preprint can be found here: https://arxiv.org/abs/2208.03263


## Code usage

For replication, use the Final_plots.ipynb notebook, which will guide you through the process that lead to the figures displayed in the paper. As the computation of the numerical simulations takes long for large networks, we computed them beforehand and load them from /results. To replicate these simulations of the network growth interventions for different parameter settings, use the Generate_network_interventions.ipynb notebook.

## Acknowledgements
This work was partially supported by funding from the Ministry of Culture and Science (MKW) of the German State of North Rhine- Westphalia ("NRW Rückkehrprogramm") and under the Excellence Strategy of the Federal Government and the Länder.