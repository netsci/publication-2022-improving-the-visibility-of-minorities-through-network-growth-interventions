#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 14:20:54 2021

@author: leonie
"""

from collections import defaultdict
from typing import Dict, Tuple, Set, Union
from faircom.utils import flatten_dict_values
import networkx as nx
import numpy as np
import pickle as pkl



def generate_pah_growth_block_model_not_size_corrected(
    initial_graph: nx.Graph,
    N: int,
    m: int,
    h: float,
    p_b:float,
    minority_fraction: float
) -> nx.Graph:
    """
    Creates a random graph, which starts growing from an initial graph which is the input, in which ties are formed based on homophily, preferential attachment.

    The initial graph is the initialisation of the model. New nodes arrive to the network and m ties are drawn to existing nodes in the graph. For each of these ties, the potential target node is chosen from this set by preferential attachment (favoring high degree nodes) and homophily (favoring nodes with the same (different) attribute for high (low) h).

    Parameters
    ----------
    N : int
        Number of nodes
    m : int
        Number of edges to attach from a new node to existing nodes
    h : float [0.,1,]
        Homophily parameter in the interval [0.,1.]. From the set of possible target nodes for a new node, similar (different) nodes with regards to their group (not block!) membership are preferred for high (low) values of h.
    p_b : float [0.,1.]
        Probability of connecting to nodes within the own block. The overall graph only creates the overall clusters if p_b > 0.5. Here irrelevant, as we are not creating different blocks, therefore set as 1.
    minority_fraction: float
        final minority fraction at the end of growth

    Returns
    -------
    G : nx.Graph
        The result of the simulation
    degree_dynamic : dict
        The degree sequence for every node of the growth
    """
   

    # Initialize final graph
    graph = initial_graph.copy()
    intial_number_of_nodes = len(graph.nodes())
    N_diff = N - intial_number_of_nodes
    initial_minority = _minority_array_(graph)
    minority_diff = int(N*minority_fraction - np.count_nonzero(initial_minority))
    assert minority_diff > 0, "minority fraction is already  equal to or larger than the intended end minority fraction"

    # Initialize node attributes
    #   node_block[i]: gives block that node i belongs to
    #   node_minority[i]: states (as bool) whether node i is minority member
    #   target_nodes[b]: holds a set of active, i.e., those that have been added, nodes for block b
    initial_blocks, target_nodes = _get_targets_(graph)
    
    # add missing minority nodes 
    missing_nodes = np.full((N_diff), False)
    missing_nodes[0:minority_diff] = True
    node_minority = np.concatenate((initial_minority,missing_nodes))
    node_block = np.concatenate((initial_blocks,missing_nodes))
    graph.add_nodes_from(list(range(intial_number_of_nodes,N)),minority=None)


    # Determine intra- and inter-block edges
    edge_intra = np.random.random((N_diff,m)) < p_b

    # Add nodes in a random order
    # Otherwise, first s_b[0] * N nodes would all be minority etc.
    # heavily favoring them in terms of preferential attachment
    node_sequence = np.random.permutation(N_diff)
    
    degree_dynamic = defaultdict(dict)
    
    existing_nodes = [k for node_block in target_nodes.keys() for k in list(target_nodes[node_block])]
    time_step = 1
    

    for node in node_sequence:
        node = intial_number_of_nodes+node
        # if node in target_nodes[node_block[node]]: # One of the initial nodes
        #     continue
        neighbors = set([node])
        
        
        all_initial_nodes = flatten_dict_values(target_nodes)
        # pick first target
        target = _pick_pa_h_target(
            graph=graph,
            source=node,
            node_targets=all_initial_nodes,
            node_minority=node_minority,
            homophily=h)
        neighbors.add(target)
        graph.add_edge(node, target)
        block = node_block[target]
        node_block[node]=block
        graph.add_node(node, minority = node_minority[node], block_size = block )
        for edge in range(m-1):
            targets = {}
            if edge_intra[node-intial_number_of_nodes, edge]:
                targets = target_nodes[block]  
                # @TODO: Create block -> node reference
                # @TODO: Add artificial nodes dynamically based on need (instead of all at once)
            else:
                targets = {target\
                    for block, block_targets in target_nodes.items() if block != node_block[node]\
                        for target in block_targets}
            targets = targets.difference(neighbors)
            # @TODO: Replace by np.random.choice
            target = _pick_pa_h_target(
                graph=graph,
                source=node,
                node_targets=targets,
                node_minority=node_minority,
                homophily=h)
            neighbors.add(target)
            graph.add_edge(node, target)
        target_nodes[node_block[node]].add(node)
        
        for n_ in existing_nodes:
            degree_dynamic[n_][time_step] = graph.degree(n_)
        
        existing_nodes.append(node)
        time_step +=1
    return graph, degree_dynamic


def _pick_pa_h_target(
        graph: nx.Graph,
        source: int,
        node_targets: Set[int],
        node_minority: np.ndarray,
        homophily: float) -> Union[int, None]:
    """Picks a random target node based on the homophily/preferential attachment dynamic.

    Args:
        G (nx.Graph): Current graph instance
        source (int): Newly added node
        target_set (Set[int]): Potential target nodes in the graph
        minority_nodes (Set[bool]): Set of minority nodes
        homophily (float): Effect of homophily.
            If high, nodes with same group membership are more likely to connect to one another.

    Returns:
        int or None: Target node that an edge should be added to
    """
    # Collect probabilities to connect to each node in target_list
    target_prob = {}
    prob_sum = 0.
    for target in node_targets:
        # Effect of preferential attachment
        target_prob[target] = graph.degree(target) + 1e-10
        # Homophily effect
        if node_minority[source] ^ node_minority[target]:
            target_prob[target] *= 1 - homophily # Separate groups
        else:
            target_prob[target] *= homophily # Same groups
        # Track sum on the fly
        prob_sum += target_prob[target]

    # Find final target by roulette wheel selection
    cum_sum = 0.
    chance = np.random.random()
    # Increase cumsum by node probabilities
    # High probabilities take more space
    # Respective nodes are thus more likely to be hit by the random number
    # Cumsum exceeds the selected random number if a hit occurs
    # A node with 3/5 of the total prob., will be hit in 3/5 of the cases
    for target in node_targets:
        cum_sum += target_prob[target] / prob_sum
        if cum_sum > chance:
            return target
    return target


def _minority_array_(graph):
    """ Returns boolean array with True for minority nodes"""
    node_array = np.full(len(graph.nodes()), False)
    for node, data in graph.nodes(data=True):
        if data.get("minority") == 1:
            node_array[node] = True
    return node_array


def _get_targets_(graph):
    """ Returns array with array[node]=block """
    node_array=[]
    target_nodes =  defaultdict(set)
    for node, data in graph.nodes(data=True):
        block = data.get("block")
        node_array.append(block)
        target_nodes[block].add(node)
    return np.array(node_array), target_nodes



class ParamsSetter:
    # pylint: disable=no-member
    def parameters_set(self, params):
        assert len(params) == len(self.parameters_mapping)
        for key, value in params.items():
            if key in self.parameters_mapping:
                setattr(self, key, value)
            else:
                raise KeyError(f"The key {key} is not valid for "+str(self.__class__))
    # pylint: enable=no-member


class HomophilyGrowthGenerator2(ParamsSetter):
  
    def __init__(self):
        super().__init__()
        self.graph = nx.Graph()
        self.n = 0
        self.m = 0
        self.homophily = 0.5
        self.attachement_prob = 0.5
        self.minority_fraction = 0.2
        self.save_degree = False
    

    @property
    def parameters_mapping(self):
        return {
            'graph': 'graph',
            'n' : 'n',
            'm' : 'm',
            'attachement_prob' : 'attachement_prob',
            'homophily' : 'homophily',
            'minority_fraction': 'minority_fraction',
            'save_degree': "save_degree"}

    def execute(self,*args, **kwargs):
        return generate_pah_growth_block_model_not_size_corrected(initial_graph = self.graph,  N = self.n,
                                m = self.m,
                                h= self.homophily,
                                p_b = self.attachement_prob,
                                minority_fraction = self.minority_fraction,
                                save_degree = self.save_degree)


