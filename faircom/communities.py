import numpy as np
import numpy.random as rand
import random as rnd
import queue as q
import pandas as pd

from faircom.utils import _minority_nodes_
from networkx.algorithms.community.quality import modularity
from networkx.algorithms.assortativity import attribute_assortativity_coefficient



def average_degree(graph):
    arr = [val for key,val in graph.degree(graph)]
    mean = np.mean(arr/(graph.number_of_nodes()-1))
    std = np.std(arr/(graph.number_of_nodes()-1))
    return mean, std



def top_k_by_class(values, graph, k):
    min_nodes =  _minority_nodes_(graph)
    order = [node for node, val in sorted(values, key=lambda x: x[1], reverse=True)]
    top_k_total = order[:k]
    top_k_min = set.intersection(set(top_k_total), set(min_nodes))
    min_k = len(top_k_min)
    min_total = len(min_nodes)
    return {
        f'min_frac_{k}' : min_k/k,
        f'min_total_{k}' : min_total,
        }



def _extract_groups_(graph):
    """Returns dataframe of lists of community nodes"""
    nodes = [[node,data.get("block")] for node, data in graph.nodes(data=True)]
    df = pd.DataFrame(nodes, columns=["node", "block"]).groupby("block").node.apply(pd.Series.tolist)
    return df

class AbstractCentralityMeasure:
    def __init__(self, mode, com, **kwargs):
        assert len(mode)>0
        if isinstance(mode, str):
            mode=[mode]

        for val in mode:
            l=["average", "top_k", "distr", "top_cumsum", "top_places", "distr_cumsum", "raw", ""]
            if not val in l:
                raise ValueError("mode should be in " + str(l))

        self.mode = mode
        if "top_k" in mode:
            self.k = kwargs['k']
        if "minlength" in kwargs:
            self.minlength = kwargs["minlength"]
        
        self.communities = com

    def execute(self, graph):
        global_values = self.get_values(graph)
        if self.communities == "global_centrality":
            communities = _extract_groups_(graph)
            subgraph_list = {"community"+str(index): graph.subgraph(value) for index, value in communities.items()}
            value_list = [[global_values[i] for i in subgraph.nodes()] for index, subgraph in subgraph_list.items()]
        if self.communities == "local_centrality":
            communities = _extract_groups_(graph)
            subgraph_list = {"community"+str(index): graph.subgraph(value) for index, value in communities.items()}
            value_list = [self.get_values(subgraph) for index, subgraph in subgraph_list.items()]
        if self.communities == "full_graph":
            subgraph_list = {"full_graph": graph}    
            value_list = [list(global_values)]
        results={}
        for values, (index, subgraph) in zip(value_list,subgraph_list.items()):
            if "top_k" in self.mode:
                results = {**results, **self.prepend(index, top_k_by_class(values, subgraph, self.k))}
        return results

    def prepend(self, index, d):
        prefix = self.name +"_"+self.communities 
        return {prefix+"_"+str(index)+"_"+key:value for key, value in d.items()}

class EdgeCountByClass(AbstractCentralityMeasure):
    name="edge"
    def get_values(self, graph):
        m_filter = ~graph.vp.minority.get_array().astype(bool)
        #m_filter = ~_get_filter_min_(graph)

        edges = graph.get_edges()
        left_values =m_filter[edges[:,0]]
        right_values=m_filter[edges[:,1]]
        return np.bincount(left_values + 2 * right_values, minlength=3)

class MinorityFraction():
    def execute(self, graph):
        minority_nodes = set(_minority_nodes_(graph))
        minority_fraction = len(minority_nodes)/graph.number_of_nodes()
        return {'minority_measured' : minority_fraction}

class NumEdges():
    def execute(self, graph):
        return {'num_edges' : graph.num_edges()}



    

class DegreeByClass(AbstractCentralityMeasure):
    name = 'degree'
    def get_values(self, graph):
        return list(graph.degree)


class JoinMeasures():
    def __init__(self, measures):
        self.measures = measures

    def execute(self, graph):
        results={}
        for measure in self.measures:
            new_result = measure.execute(graph)
            results = {**results, **new_result}
        return results

class DegreeDistributionByClass(AbstractCentralityMeasure):

    def execute(self, graph):
        degrees=graph.degree(np.arange(graph.num_vertices()))

        indices=_minority_nodes_(graph)

        min_centrality = degrees[indices]
        maj_centrality = degrees[~indices]
        print(self.minlength)
        return {'degree_0' : np.bincount(maj_centrality.astype(np.int),minlength=self.minlength),
                'degree_1' : np.bincount(min_centrality.astype(np.int),minlength=self.minlength)}
    
class Modularity():
    def execute(self, graph):
        minority_nodes = set(_minority_nodes_(graph))
        mod = modularity(graph, [minority_nodes, set(set(graph.nodes)-minority_nodes)])
        return {'modularity_measured' : mod}
    
class Assortativity():
    def execute(self, graph):
        ass = attribute_assortativity_coefficient(graph,"minority")
        return {'assortativity_measured' : ass}


    
class Assortativity_per_group():
    def execute(self, graph):
        df = _extract_groups_(graph)
        ass = [attribute_assortativity_coefficient(graph.subgraph(value),"minority") for index, value in df.items()]
        return {'assortativity_per_group_measured' : ass}
    
    
