from itertools import chain
import pandas as pd
import numpy as np

def _minority_nodes_(graph):
    """ Returns list of minority nodes"""
    nodes = [node for node, data in graph.nodes(data=True) if data.get("minority") == 1]
    return nodes



def _get_filter_min_(graph):
    """ Returns a boolean array with True for the minority"""
    filter_min = [(data.get("minority") == 1) for node, data in graph.nodes(data=True)]
    return filter_min


def flatten_dict_values(d):
    return list(set(chain.from_iterable(d.values())))

def prepare_data(df_slice, coms, k): 
    name = "degree"
    results_plotting = []
    curr_df = df_slice
    for i in range(len(coms)):
        res_global = name + "_" + "global_centrality" + "_" + "community" + str(i) + "_min_frac_" + str(k)
        res_local = name + "_" + "local_centrality" + "_" + "community" + str(i) + "_min_frac_" + str(k)
        min_total = name + "_" + "local_centrality" + "_" + "community" + str(i) + "_min_total_" + str(k)
        current = curr_df.loc[:,[min_total,res_global,res_local]]
        current.columns = ["min_total","res_global","res_local"]
        results_plotting.append(current)
    results = pd.concat(results_plotting, axis = 1, keys=coms)
    full_graph = name + "_full_graph_full_graph_min_frac_"+str(k)
    results["full_graph"] = curr_df.loc[:,full_graph]
    results["homophilies"] = df_slice["homophily"]
    results["run"] = df_slice["run"]
    if "graph" in df_slice.columns:
        results["graph"] = df_slice["graph"]
    if "minority_fractions" in df_slice.columns:
        results["minority_fractions"] = df_slice["minority_fractions"]
    if "block_sizes" in df_slice.columns:
        results["block_sizes"] = df_slice["block_sizes"]
    if "graph_id" in df_slice.columns:
        results["graph_id"] = df_slice["graph_id"]
    if "attachement_prob" in df_slice.columns:
        results["attachement_prob"] = df_slice["attachement_prob"]
    if "minority_fraction" in df_slice.columns:
        results["minority"] = df_slice["minority_fraction"]
    return results


def calculate_mean(index, values, key, result, com = None , name = None ):
    m_place=[]
    s_place=[]
    for h in values:
        if name == "full_graph":
            #result.loc[:,"tmp"] = result.loc[:,name].iloc[boolean_arr]
            result.loc[:,"tmp"] = result.loc[(result.loc[:,key]==h),name]
        else:
            #result.loc[:,"tmp"] = result.loc[:,(com,name)].iloc[boolean_arr]
            #result.loc[:,"tmp"] = result.loc[(result.loc[:,key]==h),(com,name)]
            b = result.loc[(result.loc[:,key]==h),"graph_id"]
            a = result.loc[(result.loc[:,key]==h),(com,name)]
            df = pd.concat([a,b], axis=1)
            df.rename(columns = {(com,name):'values'}, inplace = True)
        #m_place.append(result.groupby(["graph_id"])["tmp"].mean().dropna().iloc[index])
        #s_place.append(result.groupby(["graph_id"])["tmp"].std().dropna().iloc[index])
        m_place.append(float(df.groupby(["graph_id"]).mean().iloc[index]))
        s_place.append(float(df.groupby(["graph_id"]).std(ddof=0).iloc[index]))
    return m_place, s_place
    #ax=plt.gca()
    #ax.set_aspect('equal')
    
    
def analytical_prediction(homophily, minority_fraction):
    # group a is the minority
    pa = minority_fraction
    pb = 1 - pa
    
    daa = homophily
    dab = 1 - daa
    
    A = -1
    B = 2 + 2*pa*daa - pa*dab - dab*pb + 2*dab 
    C = -4*pa*daa + 2*pa*dab - 4*dab - 2*daa*dab*pa + 2*(dab**2)*pb
    D = +4*daa*dab*pa
    
    p = [A, B, C, D]
    
    ca = np.roots(p)[1] #only one root is acceptable
    
    beta_a = (daa*pa*(2-ca) + ca *dab*pb) / (ca*(2-ca))
    beta_b = 1 - (pb*(1-beta_a))/(2 - 2*beta_a-pa)
    
    return beta_a, beta_b


def analytical_prediction_2(homophily_1, minority_fraction_1, homophily_2, minority_fraction_2):
    # group a is the minority
    pa = minority_fraction_1
    pb = 1 - pa
    
    daa = homophily_1
    dab = 1 - daa
    
    pa_2 = minority_fraction_2
    pb_2 = 1 - pa_2
    
    daa_2 = homophily_2
    dab_2 = 1 - daa_2
    
    
    A = -1
    B = 2 + 2*pa*daa - pa*dab - dab*pb + 2*dab 
    C = -4*pa*daa + 2*pa*dab - 4*dab - 2*daa*dab*pa + 2*(dab**2)*pb
    D = +4*daa*dab*pa
    
    p = [A, B, C, D]
    
    ca = np.roots(p)[1] #only one root is acceptable
    
    #beta_a = (daa_2*pa_2)/(daa*ca+dab*(2-ca))+ (dab_2*pb_2) / (daa*(2-ca)+dab*ca)
    #beta_b = (daa_2*pb_2)/(daa*(2-ca)+dab*ca)+ (dab_2*pa_2) / (daa*ca+dab*(2-ca))
    
    beta_a = (daa_2*pa_2+ dab_2*pb_2) 
    beta_b = (daa_2*pb_2+dab_2*pa_2) 
    
    
    return beta_a, beta_b


def final2quota(x):
    """
    x is the final minority fraction and y is the quota that is applied to the new incoming nodes to achieve that fraction
    """
    y = 2*x-0.1
    return round(y,1)



            
        