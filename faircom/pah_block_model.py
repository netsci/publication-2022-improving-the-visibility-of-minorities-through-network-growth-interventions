from collections import defaultdict
from typing import Dict, Tuple, Set, Union
import networkx as nx
import numpy as np
import pickle as pkl

def generate_pah_block_model(
    N: int,
    m: int,
    h: float,
    p_b: float,
    s_b: np.ndarray,
    f_b: np.ndarray,
    save_degree = False
) -> nx.Graph:
    """
    Creates a random graph in which ties are formed based on homophily, preferential attachment and (potential) block membership.

    The graph is initialized with m nodes per block. Newly arriving nodes are assigned a block (based on s_b) and binary attribute (based on that block's f_b). Afterwards, m ties are drawn to existing nodes in the graph. For each of these ties, the potential target nodes are drawn from the node's own block (with prob. p_b) or the nodes from all other blocks (1 - p_b). Finally, the target is chosen from this set by preferential attachment (favoring high degree nodes) and homophily (favoring nodes with the same (different) attribute for high (low) h).

    Parameters
    ----------
    N : int
        Number of nodes
    m : int
        Number of edges to attach from a new node to existing nodes
    h : float [0.,1,]
        Homophily parameter in the interval [0.,1.]. From the set of possible target nodes for a new node, similar (different) nodes with regards to their group (not block!) membership are preferred for high (low) values of h.
    p_b : float [0.,1.]
        Probability of connecting to nodes within the own block. The overall graph only creates the overall clusters if p_b > 0.5.
    s_b : np.ndarray of n_block floats
        Size share of overall population for each block. The elements need to sum up to one. s[b] = 0.1 means that 10% of the general population will be assigned to block b.
    f_b : np.ndarray of n_block floats
        Minority shares within each block. f_b[b] = 0.4 would result in 40% of the population of block b to be members of the minority. Values can range from 0. to 1.

    Returns
    -------
    G : nx.Graph
        The result of the simulation

    Notes
    _______
    Nodes are assigned to blocks and minority membership in order of their id. That is, for two blocks with sizes 10% and 90% and N=1,000, the nodes with id 0 to 99 belong to the first block. If the minority share for that block is set to 10%, the first 10% within that block, i.e., ids 0 to 9 are minority nodes.
    When formally introducing the nodes to the final network, they are shuffled. The node with id 0 is therefore not necessarily the first one to connect to the graph!
    The m nodes per block that make up the initial graph create links to other nodes too, such that no node is disconnected from the graph.
    """
    # Sanity checks
    assert len(s_b) == len(f_b), f"s_b and f_b should have the same dimension, but found {len(s_b)} and {len(f_b)}."
    assert sum(s_b) == 1., "Elements in s_b should add up to 1."

    # Initialize final graph
    graph = nx.Graph()
    
    degree_dynamic = defaultdict(dict)

    # Initialize node attributes
    #   node_block[i]: gives block that node i belongs to
    #   node_minority[i]: states (as bool) whether node i is minority member
    #   target_nodes[b]: holds a set of active, i.e., those that have been added, nodes for block b
    node_block, node_minority, target_nodes = _init_node_atts(
        N=N, m=m,
        s_b=s_b, f_b=f_b)

    # Add all nodes to the graph object
    # If they can actually be targeted is stored in target_nodes
    graph.add_nodes_from([(node, {"minority": node_minority[node], "block": node_block[node]}) for node in range(N)])

    # Determine intra- and inter-block edges
    edge_intra = np.random.random((N,m)) < p_b

    # Add nodes in a random order
    # Otherwise, first s_b[0] * N nodes would all be minority etc.
    # heavily favoring them in terms of preferential attachment
    node_sequence = np.random.permutation(N)
    
    existing_nodes = [k for node_block in target_nodes.keys() for k in list(target_nodes[node_block])]
    time_step = 1

    for node in node_sequence:
        
        # if node in target_nodes[node_block[node]]: # One of the initial nodes
        #     continue
        neighbors = set([node])
        # Add edges
        for edge in range(m):
            targets = {}
            if edge_intra[node, edge]:
                targets = target_nodes[node_block[node]]
                # @TODO: Create block -> node reference
                # @TODO: Add artificial nodes dynamically based on need (instead of all at once)
            else:
                targets = {target\
                    for block, block_targets in target_nodes.items() if block != node_block[node]\
                        for target in block_targets}
            targets = targets.difference(neighbors)
            # @TODO: Replace by np.random.choice
            target = _pick_pa_h_target(
                graph=graph,
                source=node,
                node_targets=targets,
                node_minority=node_minority,
                homophily=h)
            neighbors.add(target)
            graph.add_edge(node, target)
        target_nodes[node_block[node]].add(node)
        if save_degree == True:
            
            for n_ in existing_nodes:
                degree_dynamic[n_][time_step] = graph.degree(n_)
            
            existing_nodes.append(node)
            time_step +=1
    
  #  if save_degree == True:
  #      with open('p1.pkl', 'wb') as f:
 #           pkl.dump(degree_dynamic, f)
 #       with open('minority1.pkl', 'wb') as f:
   #         pkl.dump(graph.nodes('minority'), f)
    return graph, degree_dynamic

def _init_node_atts(
    N: int,
    m: int,
    s_b: np.ndarray,
    f_b: np.ndarray
) -> Tuple[np.ndarray, np.ndarray, Dict[int, Set[int]]]:
    # Nodes that are present in the graph initially
    target_nodes = defaultdict(set)
    # Assigns a block to each node
    node_to_block = np.full((N), 0)
    # Binary mask to store minority membership
    node_to_minority = np.full((N), False)

    for block, node_share in enumerate(s_b):
        node_lb = int(N*sum(s_b[:block])) # lower index bound
        node_block_ub = int(node_lb + (N * node_share)) # upper bound for block
        node_min_ub = int(node_lb + (N * f_b[block] * node_share)) # upper bound for minorities

        # Assign minority status and block
        node_to_block[node_lb:node_block_ub] = block
        node_to_minority[node_lb:node_min_ub] = True

        # Add m random nodes for the start
        for target in np.random.choice(range(node_lb, node_block_ub), size=m, replace=False):
            target_nodes[block].add(target)

    return node_to_block, node_to_minority, target_nodes

def _pick_pa_h_target(
        graph: nx.Graph,
        source: int,
        node_targets: Set[int],
        node_minority: np.ndarray,
        homophily: float) -> Union[int, None]:
    """Picks a random target node based on the homophily/preferential attachment dynamic.

    Args:
        G (nx.Graph): Current graph instance
        source (int): Newly added node
        target_set (Set[int]): Potential target nodes in the graph
        minority_nodes (Set[bool]): Set of minority nodes
        homophily (float): Effect of homophily.
            If high, nodes with same group membership are more likely to connect to one another.

    Returns:
        int or None: Target node that an edge should be added to
    """
    # Collect probabilities to connect to each node in target_list
    target_prob = {}
    prob_sum = 0.
    for target in node_targets:
        # Effect of preferential attachment
        target_prob[target] = graph.degree(target) + 1e-10
        # Homophily effect
        if node_minority[source] ^ node_minority[target]:
            target_prob[target] *= 1 - homophily # Separate groups
        else:
            target_prob[target] *= homophily # Same groups
        # Track sum on the fly
        prob_sum += target_prob[target]

    # Find final target by roulette wheel selection
    cum_sum = 0.
    chance = np.random.random()
    # Increase cumsum by node probabilities
    # High probabilities take more space
    # Respective nodes are thus more likely to be hit by the random number
    # Cumsum exceeds the selected random number if a hit occurs
    # A node with 3/5 of the total prob., will be hit in 3/5 of the cases
    for target in node_targets:
        cum_sum += target_prob[target] / prob_sum
        if cum_sum > chance:
            return target
    return target


class ParamsSetter:
    # pylint: disable=no-member
    def parameters_set(self, params):
        assert len(params) == len(self.parameters_mapping)
        for key, value in params.items():
            if key in self.parameters_mapping:
                setattr(self, key, value)
            else:
                raise KeyError(f"The key {key} is not valid for "+str(self.__class__))
    # pylint: enable=no-member


class HomophilyGenerator(ParamsSetter):
    """
    Generator class for homophilic random graph with communities using BA preferential attachment model.


    Parameters
    ----------
    N : int
        Number of nodes
    m : int
        Number of edges to attach from a new node to existing nodes
    h : float [0.,1,]
        Homophily parameter in the interval [0.,1.]. From the set of possible target nodes for a new node, similar (different) nodes with regards to their group (not block!) membership are preferred for high (low) values of h.
    p_b : float [0.,1.]
        Probability of connecting to nodes within the own block. The overall graph only creates the overall clusters if p_b > 0.5.
    s_b : np.ndarray of n_block floats
        Size share of overall population for each block. The elements need to sum up to one. s[b] = 0.1 means that 10% of the general population will be assigned to block b.
    f_b : np.ndarray of n_block floats
        Minority shares within each block. f_b[b] = 0.4 would result in 40% of the population of block b to be members of the minority. Values can range from 0. to 1.

    Returns
    -------
    G : nx.Graph
        The result of the simulation

    Notes
    _______
    Nodes are assigned to blocks and minority membership in order of their id. That is, for two blocks with sizes 10% and 90% and N=1,000, the nodes with id 0 to 99 belong to the first block. If the minority share for that block is set to 10%, the first 10% within that block, i.e., ids 0 to 9 are minority nodes.
    When formally introducing the nodes to the final network, they are shuffled. The node with id 0 is therefore not necessarily the first one to connect to the graph!
    The m nodes per block that make up the initial graph create links to other nodes too, such that no node is disconnected from the graph.
    """
    def __init__(self):
        super().__init__()
        self.n = 0
        self.m = 0
        self.homophily = 0.5
        self.attachement_prob = 0.5
        self.block_sizes = np.array([0.5,0.5])
        self.minority_fractions = np.array([0.5,0.5])
        self.save_degree = False
    

    @property
    def parameters_mapping(self):
        return {
            'n' : 'n',
            'm' : 'm',
            'attachement_prob' : 'attachement_prob',
            'minority_fractions':'minority_fractions',
            'homophily' : 'homophily',
            'block_sizes':'block_sizes',
            'save_degree': 'save_degree'}

    def execute(self,*args, **kwargs):
        return generate_pah_block_model(N = self.n,
                                m = self.m,
                                h= self.homophily,
                                p_b = self.attachement_prob,
                                s_b = self.block_sizes,
                                f_b = self.minority_fractions,
                                save_degree = self.save_degree)


